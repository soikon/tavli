package gr.soikon.domain;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsolePlayer extends Player {

    private String name;
    private State state;
    public Scanner scanner;
    private Color color;

    public State getState()
    {
        return this.state;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public ConsolePlayer() {
        this.name = "PC";
        this.scanner = new Scanner(System.in);
        this.state = new State();
    }
    public ConsolePlayer(Color color) {
        this.name = "PC";
        this.scanner = new Scanner(System.in);
        this.state = new State();
        this.color=color;
    }
    public void play(State state, State opponent) {
        int[] dices = rollDices();
        move(dices, state, opponent);
        if(dices[0]==dices[1])
        {
            move(dices, state, opponent);
        }
    }

    public void move(int[] dices, State state, State opponent) {
        for (int i = 1; i < 3; i++)
        {
            boolean flag = false;
            int destination1 = 0;
            int move1 = 0;
            while(!flag)
            {
                System.out.println("Which counter do you want to move?");
                move1 = scanner.nextInt();
                while(this.state.getPositions()[move1] == 0|| this.state.getOccupied()[move1] || move1 < 0 || move1 > 23)
                {
                    System.out.println("Invalid action, please try again!");
                    move1=scanner.nextInt();
                }
                System.out.println("Where do you want to move it?");
                destination1=scanner.nextInt();
                if(destination1<24)
                {
                    if (!opponent.getBlocked()[23 - destination1])
                    {
                        flag = true;
                        while(destination1 - move1 != dices[0] && destination1 - move1 != dices[1])
                        {
                            System.out.println("Invalid action, please try again!");
                            destination1=scanner.nextInt();
                        }
                    }

                    else
                    {
                        System.out.println("Position blocked!");
                    }
                }
                else
                {
                    while(destination1 - move1 != dices[0] && destination1 - move1 != dices[1])
                    {
                        System.out.println("Invalid action, please try again!");
                        destination1=scanner.nextInt();
                    }
                }
            }

            state = new State(state.getPositions(), opponent);

            state.removeCounter(move1);
            if(destination1<24)
            {
                state.addCounter(destination1);
            }
        }

    }
}
