package gr.soikon.domain;

import gr.soikon.tavli.Game;

import java.util.ArrayList;

public class MiniMaxPlayer extends Player {

    private State state;
    private String name;
    private Color color;
    //private State opponent;

    public MiniMaxPlayer() {
        this.state = new State();

    }
    public MiniMaxPlayer(Color color) {
        this.state = new State();
        this.color=color;

    }
    public void play(ConsolePlayer opponent) {
        int[] dices = rollDices();
        if (this.getState().getCounters() > 1) {
            ArrayList<State[]> states = this.state.getAllChildren(dices[0],dices[1], opponent.getState());
            State[] best =miniMax(states);
            setState(best[0], opponent.getState());
            opponent.setState(best[1], best[0]);

            //αρχη δοκιμής
            //  if (this.getState().getCounters() != 0) {
            if (dices[0] == dices[1]) {
                if (this.getState().getCounters() > 1) {
                    states = this.state.getAllChildrenDouble(dices[0], opponent.getState());
                    /*
                     setState(states.get(0)[0], opponent.getState());
                     opponent.setState(states.get(0)[1], states.get(0)[0]);
                     */
                    best = miniMax(states);
                    setState(best[0], opponent.getState());
                    opponent.setState(best[1], best[0]);

                } else if (this.getState().getCounters() == 1) {
                    ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());

                    /*
                     setState(states.get(0)[0], opponent.getState());
                     opponent.setState(states.get(0)[1], states.get(0)[0]);
                     */
                    best = max(state);
                    setState(best[0], opponent.getState());
                    opponent.setState(best[1], best[0]);
                }
            }
            // }
        } else if (this.getState().getCounters() == 1) {
            ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());
            State[] best = max(state);
            setState(best[0], opponent.getState());
            opponent.setState(best[1], best[0]);
        }
    }
    public void play(Player opponent) {
        int[] dices = rollDices();
        if (this.getState().getCounters() > 1) {

            //
            ArrayList<State[]> states = this.state.getAllChildren(dices[0], dices[1], opponent.getState());
            State[] best =miniMax(states);
            setState(best[0], opponent.getState());
            opponent.setState(best[1], best[0]);

            //αρχη δοκιμής
            //  if (this.getState().getCounters() != 0) {
            if (dices[0] == dices[1]) {
                if (this.getState().getCounters() > 1) {
                    states = this.state.getAllChildrenDouble(dices[0], opponent.getState());
                    /*
                     setState(states.get(0)[0], opponent.getState());
                     opponent.setState(states.get(0)[1], states.get(0)[0]);
                     */
                    best = miniMax(states);
                    setState(best[0], opponent.getState());
                    opponent.setState(best[1], best[0]);

                } else if (this.getState().getCounters() == 1) {
                    ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());

                    /*
                     setState(states.get(0)[0], opponent.getState());
                     opponent.setState(states.get(0)[1], states.get(0)[0]);
                     */
                    best = miniMax(state);
                    setState(best[0], opponent.getState());
                    opponent.setState(best[1], best[0]);
                }
            }
            // }
        } else if (this.getState().getCounters() == 1) {
            ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());

                    /*
                     setState(states.get(0)[0], opponent.getState());
                     opponent.setState(states.get(0)[1], states.get(0)[0]);
                     */
            State[] best = miniMax(state);
            setState(best[0], opponent.getState());
            opponent.setState(best[1], best[0]);
        }
    }

    public void setState(State state, State opponent) {
        this.state = state;
        this.state.setConditions(opponent);

    }

    public State getState() {
        return this.state;
    }
    /*
     * public void setOpponent(State opponent) { this.opponent=opponent; }
     * public State getOpponent() { return this.opponent; }
     */

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    public State[] miniMax(ArrayList<State[]> states)
    {
        if (this.color==Color.RED)
        {
            return max(states);
        }
        else
        {
            return min(states);
        }
    }
    public static State[] max(ArrayList<State[]> states) {
        System.out.println("max player plays...");
        State[] best = states.get(0);
        int maximum = Game.evaluation(best);
        for (int i = 1; i < states.size(); i++) {
            State[] current = states.get(i);
            int eval = Game.evaluation(current);
            if (eval > maximum) {
                best = current;
            }
        }
        return best;
    }

    public static State[] min(ArrayList<State[]> states) {
        System.out.println("min player plays");
        State[] best = states.get(0);
        int min = Game.evaluation(best);
        for (int i = 1; i < states.size(); i++) {
            State[] current = states.get(i);
            int eval = Game.evaluation(current);
            if (eval < min) {
                best = current;
            }
        }
        return best;
    }

}