package gr.soikon.domain;

import java.util.ArrayList;

public class State {

    public static int INITIALCOUNTERS = 15;
    public static int POSITIONSNUMBER = 24;
    private int[] positions;
    private Condition[] conditions;//δείχνει τη κατάσταση κάθε θέσης
    private boolean[] isBlocked;//true  αν δεν μπορεί να μετακινηθεί πούλι σε αυτή τη θέση
    private boolean[] isOccupied;//true αν έχει μπλοκαριστεί ένα πούλι από τον αντίπαλο

    public State() {
        this.positions = new int[POSITIONSNUMBER];
        this.conditions = new Condition[POSITIONSNUMBER];
        this.isBlocked = new boolean[POSITIONSNUMBER];
        this.isOccupied = new boolean[POSITIONSNUMBER];
        this.setInitialState();
        this.blockPositions();

    }

    public State(int[] positions, State opponent) {
        this.positions = positions;
        this.conditions = new Condition[POSITIONSNUMBER];
        this.isBlocked = new boolean[POSITIONSNUMBER];
        this.isOccupied = new boolean[POSITIONSNUMBER];
        this.setConditions(opponent);
        this.blockPositions();

    }

    public State(int[] positions, State opponent, boolean[] occupied) {
        this.positions = positions;
        this.conditions = new Condition[POSITIONSNUMBER];
        this.isBlocked = new boolean[POSITIONSNUMBER];
        this.isOccupied = occupied;
        this.setConditions(opponent);
        this.blockPositions();

    }
    /*
     * Conditions
     * α) free: δεν υπάρχει πιονι του παίκτη εκει
     * β) dangerous: υπάρχει ένα πιόνι εκει- κίνδυνος να το "πιάσει" ο αντίπαλος
     * γ) safe:  υπάρχουν>1 πιόνια σε αυτή τη θέση, αρα είναι ασφαλής
     * δ) blocked: το πούλι είναι πιασμένο
     */

    public enum Condition {

        FREE, DANGEROUS, SAFE, BLOCKED, OCCUPIED
    }

    public void setInitialConditions() {
        for (int i = 0; i < POSITIONSNUMBER; i++) {
            conditions[i] = positions[i] == 0 ? Condition.FREE : positions[i] == 1 ? Condition.DANGEROUS : Condition.SAFE;
        }
    }

    public void setConditions(State opponent) {
        for (int i = 0; i < POSITIONSNUMBER; i++) {
            if (positions[i] == 0) {
                conditions[i] = Condition.FREE;
            } else if (positions[i] > 1) {
                conditions[i] = Condition.SAFE;
            } else {
                if (opponent.isOccupied[POSITIONSNUMBER - 1 - i]) {
                    conditions[i] = Condition.SAFE;
                } else {
                    conditions[i] = Condition.BLOCKED;
                }
            }
        }
    }

    /*
     * οι αρχικές θέσεις για το παιχνίδι "πόρτες"
     */
    public void setInitialState() {
        this.positions[0] = 15;

        setInitialConditions();
    }
    /*
     * ελέγχει που βρίσκεται το τελευταίο ελέυθερο πιόνι
     * χρήσιμο, ωστε να ξέρουμε αν μπορούμε να αφαιρέσουμε
     * πιόνια από το παιχνίδι
     */

    public int getLastCounter() {
        int i = 0;
        for (i = 0; i < POSITIONSNUMBER; i++) {
            if (this.positions[i] > 0) {
                break;
            }
        }
        return i;
    }
    /*
     *πιθανές καταστάσεις μόνο από μία ζαριά
     */

    public ArrayList<State[]> getChildren(int dice, State opponentState) {
        if (this.getCounters() != 0) {
            ArrayList<State[]> children = new ArrayList<State[]>();
            if (!this.isTerminal()) {
                for (int i = 0; i < POSITIONSNUMBER; i++) {
                    if (positions[i] > 0 && !isOccupied[i]) {
                        if (i + dice >= POSITIONSNUMBER)// ένα πούλι  στη θέση i μπορεί να βγει από το παιχνίδι
                        {
                            if (this.getLastCounter() > 16) {
                                int[] childsPositions = new int[POSITIONSNUMBER];
                                for (int runner = 0; runner < POSITIONSNUMBER; runner++)//αντιγραφη του παλιού πίνακα κατάστασης
                                {
                                    childsPositions[runner] = this.positions[runner];
                                }
                                childsPositions[i]--;//αφαίρεση του πουλιού i στη νέα κατάσταση
                                State child = new State(childsPositions, opponentState);
                                State childOpponent = new State(opponentState.getPositions(), this);
                                State[] states = new State[2];
                                states[0] = child;
                                states[1] = childOpponent;
                                children.add(states);
                            }
                        } else {

                            if (!(opponentState.isBlocked[POSITIONSNUMBER - i - dice - 1])) {
                                int[] childsPositions = new int[POSITIONSNUMBER];
                                boolean[] occup = new boolean[POSITIONSNUMBER];
                                for (int runner = 0; runner < POSITIONSNUMBER; runner++) {
                                    childsPositions[runner] = this.positions[runner];
                                }
                                childsPositions[i]--;
                                childsPositions[i + dice]++;
                                if (opponentState.conditions[POSITIONSNUMBER - i - dice - 1] == Condition.DANGEROUS) {
                                    opponentState.occupy(POSITIONSNUMBER - i - dice - 1);
                                    occup[POSITIONSNUMBER - i - dice - 1] = true;
                                }

                                State child = new State(childsPositions, opponentState);
                                State childOpponent = new State(opponentState.getPositions(), this, occup);
                                for (int j = 0; j < POSITIONSNUMBER; j++) {
                                    if (this.getPositions()[j] > opponentState.getPositions()[POSITIONSNUMBER - j - 1] && opponentState.getPositions()[POSITIONSNUMBER - j - 1] == 1) {
                                        childOpponent.isOccupied[POSITIONSNUMBER - j - 1] = true;
                                        //childOpponent.occupy(POSITIONSNUMBER-j-1);
                                        occup[POSITIONSNUMBER - i - dice - 1] = true;
                                    }
                                }
                                State[] states = new State[2];
                                states[0] = child;
                                states[1] = childOpponent;
                                children.add(states);
                            }
                        }
                    }
                }
            }
            return children;
        }
        return null;
    }

    public ArrayList<State[]> getAllChildrenDouble(int die, State opponent) {
        ArrayList<State[]> children = new ArrayList<State[]>();
        ArrayList<State[]> childrenDice = null;
        if(getChildren(die, opponent).isEmpty()){
            State[] state=new State[2];
            state[0]=this;
            state[1]=opponent;
            children.add(state);
        }
        else{
            for (int i = 0; i < 4; i++) {
                if (this.getCounters() > 0) {
                    childrenDice = getChildren(die, opponent);
                    if(!childrenDice.isEmpty())
                    {
                        for (int j = 0; j < childrenDice.size(); j++) {
                            State[] states = childrenDice.get(j);
                            children.addAll(states[0].getChildren(die, states[1]));
                        }
                        childrenDice.clear();
                    }
                }
            }
        }
        return children;
    }

    public ArrayList<State[]> getAllChildren(int dice1, int dice2, State opponent) {
        ArrayList<State[]> children = new ArrayList<State[]>();
        ArrayList<State[]> childrenDice = null, childrenDice1 = null;

        childrenDice = getChildren(dice1, opponent);
        childrenDice1 = getChildren(dice2, opponent);
        if (childrenDice.isEmpty() && childrenDice1.isEmpty()) {
            State[] states = new State[2];
            states[0] = this;
            states[1] = opponent;
            children.add(states);
        } else if (childrenDice.isEmpty() && childrenDice1.size() > 0) {
            boolean flag=false;

            for (int i = 0; i < childrenDice1.size(); i++) {
                State[] states = childrenDice1.get(i);
                if(states[0].getChildren(dice1, states[1]).size()>0)
                {
                    flag=true;
                    children.addAll(states[0].getChildren(dice1, states[1]));
                }
            }
            if(!flag)
            {
                children.addAll(getChildren(dice2, opponent));
            }
        }  else {
            boolean flag=false;
            for (int i = 0; i < childrenDice.size(); i++) {
                State[] states = childrenDice.get(i);
                if(states[0].getChildren(dice2, states[1]).size()>0)
                {
                    flag=true;
                    children.addAll(states[0].getChildren(dice2, states[1]));
                }
            }
            if(!flag)
            {
                children.addAll(getChildren(dice1, opponent));
            }
        }


        //  }


        /*
         }
         */
        /*
         else {
         for (int i = 0; i < 4; i++) {
         childrenDice = getChildren(dice1, opponent);
         for (int j = 0; j < childrenDice.size(); j++) {
         State[] states = childrenDice.get(j);
         children.addAll(states[0].getChildren(dice2, states[1]));
         }
         childrenDice.clear();
         }
         }
         */
        return children;
    }

    public void printState() {
        for (int i = 0; i < POSITIONSNUMBER; i++) {
            System.out.print("pos " + i + ": " + this.positions[i] + " ");
        }
        System.out.println();
    }

    public int getCounters() {
        int count = 0;
        for (int runner = 0; runner < positions.length; runner++) {
            count += positions[runner];
        }
        return count;
    }

    public boolean isTerminal() {
        return this.getCounters() == 0;
    }

    public void occupy(int pos) {
        this.isOccupied[pos] = true;
        this.conditions[pos] = Condition.BLOCKED;
        this.isBlocked[pos] = true;
    }

    public void release(int pos) {
        this.isOccupied[pos] = false;
        this.conditions[pos] = Condition.DANGEROUS;
    }

    public void blockPositions() {

        for (int i = 0; i < this.isBlocked.length; i++) {
            if (this.conditions[i] == Condition.SAFE) {
                this.isBlocked[i] = true;
            }
        }

    }

    public boolean[] getBlocked() {
        return this.isBlocked;
    }

    public int[] getPositions() {
        return this.positions;
    }

    public boolean[] getOccupied() {
        return this.isOccupied;
    }

    public void removeCounter(int pos) {
        this.positions[pos]--;
    }

    public void addCounter(int pos) {
        this.positions[pos]++;
    }

    public Condition[] getConditions() {
        return this.conditions;
    }
}