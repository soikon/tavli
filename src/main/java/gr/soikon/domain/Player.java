package gr.soikon.domain;

import java.util.ArrayList;
import java.util.Random;

public class Player {

    private State state;
    private String name;
    private Color color;
    // private State opponent;
    public enum Color
    {
        RED,BLACK
    }
    public Player() {
        this.state = new State();
        this.name = "NPC";
    }

    public void play(ConsolePlayer opponent) {
        int[] dices = rollDices();
        if (dices[0] == dices[1]) {
            if (this.getState().getCounters() > 1) {
                ArrayList<State[]> states = this.state.getAllChildrenDouble(dices[0], opponent.getState());
                setState(states.get(0)[0], opponent.getState());
                opponent.setState(states.get(0)[1], states.get(0)[0]);
            } else if (this.getState().getCounters() == 1) {
                ArrayList<State[]> state = this.state.getChildren(2 * (dices[0] + dices[1]), opponent.getState());
                setState(state.get(0)[0], opponent.getState());
                opponent.setState(state.get(0)[1], state.get(0)[0]);
            }
        } else {
            if (this.getState().getCounters() > 1) {
                ArrayList<State[]> states = this.state.getAllChildren(dices[0], dices[1], opponent.getState());
                setState(states.get(0)[0], opponent.getState());
                opponent.setState(states.get(0)[1], states.get(0)[0]);
            } else {
                ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());
                setState(state.get(0)[0], opponent.getState());
                opponent.setState(state.get(0)[1], state.get(0)[0]);
            }
        }
    }

    public void play(Player opponent) {
        int[] dices = rollDices();
        if (dices[0] == dices[1]) {
            if (this.getState().getCounters() > 1) {
                ArrayList<State[]> states = this.state.getAllChildrenDouble(dices[0], opponent.getState());
                setState(states.get(0)[0], opponent.getState());
                opponent.setState(states.get(0)[1], states.get(0)[0]);
            } else if (this.getState().getCounters() == 1) {
                ArrayList<State[]> state = this.state.getChildren(2 * (dices[0] + dices[1]), opponent.getState());
                setState(state.get(0)[0], opponent.getState());
                opponent.setState(state.get(0)[1], state.get(0)[0]);
            }
        } else {
            if (this.getState().getCounters() > 1) {
                ArrayList<State[]> states = this.state.getAllChildren(dices[0], dices[1], opponent.getState());
                setState(states.get(0)[0], opponent.getState());
                opponent.setState(states.get(0)[1], states.get(0)[0]);
            } else {
                ArrayList<State[]> state = this.state.getChildren(dices[0] + dices[1], opponent.getState());
                setState(state.get(0)[0], opponent.getState());
                opponent.setState(state.get(0)[1], state.get(0)[0]);
            }
        }
    }

    /*
     * public void play(Player opponent) { int[] dices = rollDices(); if
     * (this.getState().getCounters() > 1) { ArrayList<State[]> states =
     * this.state.getAllChildren(dices[0], dices[1], opponent.getState());
     * setState(states.get(0)[0], opponent.getState());
     * opponent.setState(states.get(0)[1], states.get(0)[0]); //αρχη δοκιμής //
     * if (this.getState().getCounters() != 0) { if (dices[0] == dices[1]) { if
     * (this.getState().getCounters() > 1) { states =
     * this.state.getAllChildren(dices[0], dices[1], opponent.getState());
     * setState(states.get(0)[0], opponent.getState());
     * opponent.setState(states.get(0)[1], states.get(0)[0]); } else
     * if(this.getState().getCounters()==1){ ArrayList<State[]> state =
     * this.state.getChildren(dices[0] + dices[1], opponent.getState());
     * setState(state.get(0)[0], opponent.getState());
     * opponent.setState(state.get(0)[1], state.get(0)[0]); } } // } } else
     * if(this.getState().getCounters()==1){ ArrayList<State[]> state =
     * this.state.getChildren(dices[0] + dices[1], opponent.getState());
     * setState(state.get(0)[0], opponent.getState());
     * opponent.setState(state.get(0)[1], state.get(0)[0]); }
     *
     *
     * //τέλος δοκιμής //setOpponent(states.get(0)[1]);
     *
     * } /* public void play(State state) { int[] dices=rollDices();
     * ArrayList<State>
     * states=this.state.getAllChildren(dices[0],dices[1],state); for(int
     * i=0;i<states.size();i++) { setState(states.get(i)); System.out.println(
     * "\npossible next condition "+i); this.state.printState(); }
     * setState(states.get(0));
     *
     *
     * }
     */
    public int[] rollDices() {
        int[] dice = new int[2];
        Random rand = new Random();
        dice[0] = 1 + rand.nextInt(Integer.SIZE - 1) % 6;
        dice[1] = 1 + rand.nextInt(Integer.SIZE - 1) % 6;
        System.out.println("dice1: " + dice[0] + " dice2: " + dice[1]);
        return dice;
    }

    public void setState(State state, State opponent) {
        this.state = state;
        this.state.setConditions(opponent);

    }

    public State getState() {
        return this.state;
    }
    /*
     * public void setOpponent(State opponent) { this.opponent=opponent; }
     * public State getOpponent() { return this.opponent; }
     */

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    public void setColor(Color color)
    {
        this.color=color;
    }
    public Color getColor()
    {
        return this.color;
    }
}
