package gr.soikon.tavli;

import java.util.Scanner;

import gr.soikon.domain.ConsolePlayer;
import gr.soikon.domain.Player;
import gr.soikon.domain.State;
import gr.soikon.domain.State.Condition;
import gr.soikon.graphics.Board;
import gr.soikon.graphics.Table;


public class Game {

    private Player player;
    private ConsolePlayer user;
    private Player opponent;
    private Table table,previus;
    private Board board;
    Scanner scanner;

    public Game(ConsolePlayer user, Player opponent) {
        //System.out.println("FIRST CONSTRUCTOR CALL");
        this.user = user;
        this.player = opponent;
        this.user.setName("User");
        this.player.setName("NPC");
        this.opponent = null;
        Table table=new Table(player.getState().getPositions(), user.getState().getPositions());
        board=new Board(table);
        //board = new Board(player.getState().getPositions(), user.getState().getPositions());
        scanner = new Scanner(System.in);
    }
    public Game(Player opponent, ConsolePlayer user) {
        this.user = user;
        this.player = opponent;
        this.user.setName("User");
        this.player.setName("NPC");
        this.opponent = null;
        Table table=new Table(player.getState().getPositions(), user.getState().getPositions());
        board=new Board(table);
        //board = new Board(player.getState().getPositions(), user.getState().getPositions());
        scanner = new Scanner(System.in);
    }
    public Game(Player player, Player opponent) {
        this.player = player;
        this.opponent = opponent;
        this.player.setName("NPC1");
        this.opponent.setName("NPC2");
        board = new Board(player.getState().getPositions(), opponent.getState().getPositions());
        scanner = new Scanner(System.in);
    }

    public void start() {
        if (this.opponent != null) {
            while (true) {
                // if (scanner.nextInt()==1) {
                System.out.println("red player plays");
                player.play(opponent);
                if (player.getState().isTerminal()) {
                    System.out.println("red player won");
                    break;
                }
                player.getState().printState();
                Table table=new Table(player.getState().getPositions(), opponent.getState().getPositions());
                board.clear();
                board.add(new Table(player.getState().getPositions(), opponent.getState().getPositions()));
                board.show();
                // }
                //   if (scanner.nextInt() == 1) {
                opponent.play(player);
                opponent.getState();
                System.out.println("black player plays");
                if (opponent.getState().isTerminal()) {
                    System.out.println("black player won");
                    break;
                }
                opponent.getState().printState();
                board.clear();
                board.add(new Table(player.getState().getPositions(), opponent.getState().getPositions()));
                board.show();
                //  }
            }
        }
        else
        {
            while (true) {
                //  if (scanner.nextInt()== 1) {
                // user.play(player);
                user.play(user.getState(), player.getState());
                System.out.println("player 1 plays");
                if (user.getState().isTerminal()) {
                    System.out.println(user.getName() +" won!");
                    break;
                }
                user.getState().printState();
                board.clear();
                board.add(new Table(user.getState().getPositions(), player.getState().getPositions()));
                board.show();
                //  }
                // if (scanner.nextInt()== 1) {
                player.play(user);
                System.out.println("Black player plays");
                if ( player.getState().isTerminal()) {
                    System.out.println(player.getName() + " won!");
                    break;
                }
                player.getState().printState();
                player.setState(player.getState(),user.getState());
                board.clear();
                board.add(new Table(user.getState().getPositions(), player.getState().getPositions()));
                board.show();
                //  }
            }
        }
    }
    public static int evaluation(State[] states)
    {
        State playersState=states[0];
        State opponentsState=states[1];
        int eval=(State.INITIALCOUNTERS-playersState.getCounters())*10-(State.INITIALCOUNTERS-opponentsState.getCounters())*10;
        int[] opponentsPositions=opponentsState.getPositions();
        int[] playersPositions=playersState.getPositions();
        Condition[] playersConditions=playersState.getConditions();
        Condition[] opponentsConditions=opponentsState.getConditions();
        boolean[] playersOccupied=playersState.getOccupied();
        boolean[] opponentsOccupied=opponentsState.getOccupied();
        for(int i=0;i<State.POSITIONSNUMBER;i++)
        {
            int incr=i>16?20:5;
            eval+=incr*playersPositions[i]-incr*opponentsPositions[i];
            if(playersConditions[i]==Condition.DANGEROUS)
            {
                eval-=50;
            }
            if(opponentsConditions[i]==Condition.DANGEROUS)
            {
                eval+=50;
            }
            /*
            if(playersConditions[i]==Condition.SAFE)
            {
                eval+=20;
            }
            if(opponentsConditions[i]==Condition.SAFE)
            {
                eval-=20;
            }
            if(playersConditions[i]==Condition.BLOCKED)
            {
                eval+=100;
            }
            if(opponentsConditions[i]==Condition.BLOCKED)
            {
                eval-=100;
            }
            */
            if(playersOccupied[i])
            {
                if(i > 17)
                    eval -= 300;
                else
                    eval-=200;
            }
            if(opponentsOccupied[i])
            {
                if (i >17)
                    eval += 300;
                else
                    eval+=200;
            }
        }
        return eval;
    }
}
