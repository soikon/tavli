package gr.soikon.tavli;

import gr.soikon.domain.ConsolePlayer;
import gr.soikon.domain.MiniMaxPlayer;
import gr.soikon.domain.Player;

import javax.swing.JOptionPane;

public class Tavli {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null,"Είστε ο κόκκινος παίκτης");
        JOptionPane.showMessageDialog(null,"Για να  παίξετε δείτε τα αποτελέσματα της ρήψης ζαριών στη γραμμή εντολών");
        Game game = null;
        //Player player = new MiniMaxPlayer(Player.Color.RED);
        ConsolePlayer player = new ConsolePlayer(Player.Color.RED);
        MiniMaxPlayer opponent = new  MiniMaxPlayer(Player.Color.BLACK);
        game=new Game(player, opponent);

        game.start();
    }
}
