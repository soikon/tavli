package gr.soikon.graphics;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class Board extends JFrame
{
  public static final int X=0;
  public static final int Y=0;
  public static final int WIDTH=400;
  public static final int HEIGHT=600;
  private int[] counters;
  private int[] opponentCounters;
  public Board()
  {
     this.setSize(WIDTH+50,HEIGHT+50);
     this.setTitle("board");
     this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     this.setVisible(true);
     Table table=new Table();
     this.add(table);
     /*
     JFrame frame=new JFrame();
     JLabel label=new JLabel("hello");
     frame.setSize(WIDTH+50,HEIGHT+50);
     frame.setTitle("board");
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.setVisible(true);
     Table table=new Table();
     frame.add(table);
     */
  }
  public Board(int[] counters)
  {JFrame frame=new JFrame();
  this.counters=counters;
  System.out.println("**************at board:"+this.counters.length);
  
     JLabel label=new JLabel("hello");
     frame.setSize(WIDTH+50,HEIGHT+50);
     frame.setTitle("board");
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.setVisible(true);
     Table table=new Table();
     //table.addCounters();
     frame.add(table);
        
  }
  public Board(int[] counters,int[] opponentCounters)
  {
      //JFrame frame=new JFrame();
  this.counters=counters;
  this.opponentCounters=opponentCounters;
  //System.out.println("**************at board:"+this.counters.length);
     
    this.setSize(WIDTH+50,HEIGHT+50);
     this.setTitle("board");
     this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     this.setVisible(true);
     Table table=new Table(this.counters,this.opponentCounters);
     this.add(table);
  }
  public Board(Table table)
  {
      //JFrame frame=new JFrame();
  this.counters=counters;
  this.opponentCounters=opponentCounters;
  //System.out.println("**************at board:"+this.counters.length);
     
    this.setSize(WIDTH+50,HEIGHT+50);
     this.setTitle("board");
     this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     this.setVisible(true);
     this.add(table);
  }
  public void addFrame(Table table)
  {
     this.add(table);
  }
  public void changeFrame(Table previousTable, Table newTable)
  {
      this.remove(previousTable);
      this.add(newTable);
  }
  public void clear()
  {
      this.getContentPane().removeAll();
  }
}
