package gr.soikon.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import javax.swing.JComponent;

public class Counter extends JComponent{
    private double x;
    private double y;
    private boolean red;
    public Counter()
    {
    }
    public Counter(int x,int y, boolean red)
    {
        this.x=x;
        this.y=y;
        this.red=red;
    }
    public void paintComponent(Graphics graph)
    {
        Graphics2D g2=(Graphics2D) graph;
        Ellipse2D.Double counter=new Ellipse2D.Double(x,y,WIDTH/12,WIDTH/12);
        if(red)
        {
            g2.setColor(Color.RED);
        }
        else
        {
            g2.setColor(Color.BLACK);
        }
        g2.fill(counter);
        
    }
}
