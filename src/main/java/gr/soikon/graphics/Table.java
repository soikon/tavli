package gr.soikon.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import javax.swing.JPanel;

public class Table extends JPanel {

    private int x = 5;
    private int y = 5;
    private int counters[];
    private int opponentCounters[];
    public Table() {
    }

    public Table(int[] counters, int[] opponentCounters) {
        this.counters = counters;
        this.opponentCounters=opponentCounters;
    }

    public void paintComponent(Graphics graph) {
        super.paintComponent(graph);
        Graphics2D g2 = (Graphics2D) graph;
        this.setBackground(Color.MAGENTA);
        graph.setColor(Color.DARK_GRAY);
        graph.drawRect(x, y, Board.WIDTH, Board.HEIGHT);
        // Rectangle rect=new Rectangle(5,5,Board.WIDTH,Board.HEIGHT);
        //g2.setColor(Color.BLACK);
        //g2.fillRect(5,5,Board.WIDTH,Board.HEIGHT);
        g2.setColor(Color.CYAN);
        Polygon triangle = null;
        int currentX = x;
        int currentY = y;
        for (int i = 0; i < 12; i++) {
            //���� �������
            int xPoly[] = {currentX, currentX + Board.WIDTH / 24, currentX + Board.WIDTH / 12};
            int yPoly[] = {currentY, currentY + 4 * Board.HEIGHT / 9, currentY};
            triangle = new Polygon(xPoly, yPoly, 3);
            g2.fillPolygon(triangle);
            //���� �������
            int xPolyDown[] = {currentX, currentX + Board.WIDTH / 24, currentX + Board.WIDTH / 12};
            int yPolyDown[] = {currentY + Board.HEIGHT, currentY + 5 * Board.HEIGHT / 9, currentY + Board.HEIGHT};
            triangle = new Polygon(xPolyDown, yPolyDown, 3);
            g2.fillPolygon(triangle);
            currentX = currentX + Board.WIDTH / 12;
        }
        //Line2D.Double line=new Line2D.Double(Board.WIDTH/2,0,Board.WIDTH/2,Board.HEIGHT);
        g2.setColor(Color.BLACK);
        g2.fillRect(Board.WIDTH/2,0,10,Board.HEIGHT);
        g2.drawLine(Board.WIDTH/2,0,Board.WIDTH/2,Board.HEIGHT);
        //Counter counter=new Counter(x+10,y+10,true);
        addCounters(graph);
        addLabels(graph);
        //  counter.paintComponent(graph);
    }

   

    public void addCounters(Graphics graph) {
        Graphics2D g2 = (Graphics2D) graph;
       // System.out.println("*****counters length: " + counters.length);
        for (int i = 0; i < counters.length / 2; i++) {
            if (counters[i] > 0) {
                for (int j = 0; j < counters[i]; j++) {
                    graph.setColor(Color.RED);
                    graph.fillOval(x +Board.WIDTH- (i+1) * Board.WIDTH /12+(Board.WIDTH/12)/4 , y + j * Board.WIDTH / 24, Board.WIDTH /24, Board.WIDTH /24);
                }
            }
        }
        for (int i = counters.length / 2; i < counters.length; i++) {
            if (counters[i] > 0) {
                for (int j = 0; j < counters[i]; j++) {
                    graph.setColor(Color.RED);
                    graph.fillOval(x +(i-counters.length/2) * Board.WIDTH / 12+(Board.WIDTH/12)/4, y + Board.HEIGHT - (j + 1) * Board.WIDTH /24, Board.WIDTH /24, Board.WIDTH /24);
                }
            }
        }
        for (int i = 0; i< opponentCounters.length / 2; i++) {
            if (opponentCounters[i] > 0) {
                for (int j = 0; j < opponentCounters[i]; j++) {
                    graph.setColor(Color.BLACK);
                    graph.fillOval(x +Board.WIDTH-(i+1)* Board.WIDTH / 12+(Board.WIDTH/12)/4, y+Board.HEIGHT -(j+1) * Board.WIDTH /24, Board.WIDTH /24, Board.WIDTH /24);
                }
            }
        }
        for (int i = counters.length / 2; i < counters.length; i++) {
            if (opponentCounters[i] > 0) {
                for (int j = 0; j < opponentCounters[i]; j++) {
                    graph.setColor(Color.BLACK);
                    graph.fillOval(x + ( i-counters.length/2) * Board.WIDTH / 12+(Board.WIDTH/12)/4, y +j * Board.WIDTH /24, Board.WIDTH /24, Board.WIDTH /24);
                }
            }
        }
       
    }
    public void addLabels(Graphics graph)
    {
        Graphics2D g2 = (Graphics2D) graph;
        for(int i=0;i<12;i++)
        {
            graph.setColor(Color.BLACK);
            graph.drawString(""+i,x +Board.WIDTH- (i+1) * Board.WIDTH /12+(Board.WIDTH/12)/4 , y+10);
        }
         for(int i=23;i>11;i--)
        {
            graph.drawString(""+i,x +Board.WIDTH- (24-i) * Board.WIDTH /12+(Board.WIDTH/12)/4 ,Board.HEIGHT+y);
        }
    }
    
}
